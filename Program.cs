﻿using System;
using System.Linq;
using System.Threading;
using System.Runtime.Serialization;
using System.IO;
using DDay.iCal;
using NX;
using CoreTweet;

namespace PSO2Cal
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            var unix = (Environment.OSVersion.Platform == PlatformID.Unix || Environment.OSVersion.Platform == PlatformID.MacOSX);
            var tf = unix ? ".twtokens" : "twtokens.xml";
            var home = unix ? Environment.GetEnvironmentVariable("HOME") : Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "csharp");
            var ds = new DataContractSerializer(typeof(string[]));
            var t = 
                from f in File.OpenRead(Path.Combine(home, tf)).Use()
                            select (string[])ds.ReadObject(f);
            var tokens = Tokens.Create(t[0], t[1], t[2], t[3]);
            
            var gc = new GoogleCal("bfi25n1qn3m1ma7drg2s3bb1n4@group.calendar.google.com");
            Console.WriteLine("Logging in... ({0})", DateTime.Now);
            gc.Authorize().Wait();
            Console.WriteLine("Checking connection... ({0})", DateTime.Now);
            if (gc.Check())
                while (true)
                {
                    Console.WriteLine("Fetching iCal... ({0})", DateTime.Now);
                    var ics = iCalendar.LoadFromUri(new Uri("http://akane.tukimi.glasscore.net/calendar/emergency.ics"));
                    Console.WriteLine("Updating Google Calendar... ({0})", DateTime.Now);
                    var after = ics.First().Events.Map(Tools.ToGData);
                    var before = gc.Events.Value;
                    var resa = 0;
                    var resd = 0;
                    foreach (var f in after.Filter(x => !before.Any(x.WeakEquals)))
                    {
                        if ((f.Start.DateTime - DateTime.Now) > TimeSpan.FromDays(1))
                        if (gc.Add(f))
                            resa += 1;
                    }
                    foreach (var f in before.Filter(x => !after.Any(x.WeakEquals)))
                    {
                        if ((f.Start.DateTime - DateTime.Now) > TimeSpan.Zero)
                        {
                            gc.Remove(f);
                            resd += 1;
                        }
                    }
                    Console.WriteLine("{0} items added.", resa);
                    Console.WriteLine("{0} items removed.", resd);
                    Console.WriteLine("Done. ({0})", DateTime.Now);
                    var n = DateTime.Now;
                    var next = n.AddHours(1).AddMinutes(7.0 - n.Minute);
                    Console.WriteLine("Wait until {0}.", next);
                    tokens.Try(x => x.DirectMessages.New("cannorin", string.Format("PSO2Cal: +{1} -{2} ({0})", DateTime.Now, resa, resd)));
                    Thread.Sleep((int)(next - n).TotalMilliseconds); 
                }
            else
                Console.WriteLine("Check failed, aborting.");
        }
    }
}
