﻿using System.IO;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;
using Google.Apis.Calendar.v3;
using GData = Google.Apis.Calendar.v3.Data;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Services;
using NX;
using iCal = DDay.iCal;
using System.Collections.Generic;
using System;

namespace PSO2Cal
{
    public class GoogleCal
    {
        CalendarService srv;
        readonly string clid;

        public Cached<IList<GData.Event>> Events { get; private set; }

        HashSet<EventStore> history;

        public GoogleCal(string calendarId)
        {
            clid = calendarId;
            history = new HashSet<EventStore>();
            Events = new Cached<IList<GData.Event>>(() =>
            {
                var req = srv.Events.List(clid);
                req.MaxResults = 1000;
                req.TimeMin = DateTime.Now - TimeSpan.FromDays(7);
                return req.Execute().Items;
            }
                , TimeSpan.FromHours(1)
            );
        }

        public async Task Authorize()
        {
            UserCredential crd;
            using (var x = File.OpenRead("client_secret.json"))
                crd = await GoogleWebAuthorizationBroker.AuthorizeAsync(
                    GoogleClientSecrets.Load(x).Secrets,
                    new[] { CalendarService.Scope.Calendar },
                    "user", CancellationToken.None);
            
            srv = new CalendarService(new BaseClientService.Initializer()
            {
                HttpClientInitializer = crd,
                ApplicationName = "PSO2Cal_1",
            });
        }

        public bool Check()
        {
            try
            {
                srv.Calendars.Get(clid).Execute();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool Add(GData.Event e)
        {
            var es = e.ToStore();
            var b = Events.Value.Map(Tools.ToStore);
            var o = Events.Value.Find(e.WeakEquals);
            if (history.Contains(es) || b.Any(x => x.GetHashCode() == es.GetHashCode()))
            {
                Console.WriteLine("Event already exists: {0}", e.Summary);
                return false;
            }
            else if (o.HasValue)
            {
                var s = Update(o.Value.Id, e);
                Console.WriteLine("Event updated: {0} ({1})", e.Summary, s);
                history.Add(e.ToStore());
                return true;
            }
            else
            {
                var res = srv.Events.Insert(e, clid).Execute();
                Console.WriteLine("Event added: {0} ({1})", e.Summary, res.Id);
                history.Add(e.ToStore());
                return true;
            }
        }

        public string Update(string before, GData.Event after)
        {
            var res = srv.Events.Update(after, clid, before).Execute();
            return res.Id;
        }

        public string Remove(GData.Event e)
        {
            Console.WriteLine("Event removed: {0} ({1})", e.Summary, e.Id);
            return srv.Events.Delete(clid, e.Id).Execute();
        }
    }
}

