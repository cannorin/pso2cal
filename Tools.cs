﻿using System;
using iCal = DDay.iCal;
using GData = Google.Apis.Calendar.v3.Data;
using NX;

namespace PSO2Cal
{
    public class Cached<T>
    {
        readonly Func<T> f;
        Option<T> value;
        DateTime refreshed;
        TimeSpan t;

        public T Value
        {
            get
            {
                if (!value.HasValue || (DateTime.Now - refreshed) > t)
                {
                    value = f().Some();
                    refreshed = DateTime.Now;
                    return value.Value;
                }
                else
                {
                    return value.Value;
                }
            }
        }

        public Cached(Func<T> getter, TimeSpan span)
        {
            f = getter;
            t = span;
        }
    }

    public struct EventStore
    {
        public readonly DateTime Start;
        public readonly DateTime End;
        public readonly string Summary;

        public EventStore(DateTime s, DateTime e, string m)
        {
            Start = s;
            End = e;
            Summary = m;
        }

        public override int GetHashCode()
        {
            return Start.GetHashCode() ^ End.GetHashCode() ^ Summary.GetHashCode();
        }
    }

    public static class Tools
    {

        public static bool GEquals(this GData.Event e, GData.Event f)
        {
            return e.Summary.Equals(f.Summary) && e.Start.DateTime.Value.Equals(f.Start.DateTime.Value) &&
            e.End.DateTime.Value.Equals(f.End.DateTime.Value);
        }

        public static bool WeakEquals(this GData.Event e, GData.Event f)
        {
            return e.Start.DateTime.Value.Equals(f.Start.DateTime.Value) &&
            e.End.DateTime.Value.Equals(f.End.DateTime.Value);
        }

        public static GData.Event ToGData(this iCal.IEvent e)
        {
            return new GData.Event
            {
                Summary = e.Summary,
                Start = new GData.EventDateTime
                {
                    DateTime = e.Start.Local,
                    TimeZone = e.Start.TimeZoneName
                },
                End = new GData.EventDateTime
                {
                    DateTime = e.End.Local,
                    TimeZone = e.End.TimeZoneName
                },
            };
        }

        public static EventStore ToStore(this GData.Event e)
        {
            return new EventStore(e.Start.DateTime.Value, e.End.DateTime.Value, e.Summary);
        }
    }
}

